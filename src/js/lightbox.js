
var AUTOSTART_CHAT_TIMEOUT = 2300;

function LightboxCtrl(obj)
{
    this._obj = obj; // jquery obj
    this._item = null;
    this._chat = null;
    this._shownItem = null;
    this._submitTimerid = null;
    //this._submitLastLine = null;
    this._submitUndeliveredLines = new Array();

    this._prevScrollX = 0;
    this._prevScrollY = 0;


    var that = this;
    var __init = function() // c'tor
    {
        that._chat = new SexyChatCtrl($("#chat"), CHAT_URL_PREFIX, {"me":"chatMsgMe","girl":"chatMsgGirl","typing":"chatMsgTyping "} );

        that._obj.find("#chat-input").keydown(function(e){ if( e.keyCode == 13 ) return that.__chatSubmit(e); } );
        that._obj.find("#chat-button").click( function(e){ e.preventDefault(); that._obj.find("#chat-input").focus(); that.__chatSubmit(e);} );
        that._obj.find("#lightbox-close").on("click", function(e){that.hide();} );

        document.addEventListener('keydown', function (event) {
          var e = event || window.event;
          if (e.keyCode === 27) {
            that.hide();
          }
        });
    }

    this.__cancelReply = function()
    {
        this._chat.cancelReply();

        if (this._submitTimerid) {
            clearTimeout(this._submitTimerid);
        }

        if (this._submitLastLine) {
            setMsgStatus(this._submitLastLine, "delivered");
        }
    }

    var setMsgStatus = function(msgLine, status)
    {
        var o = msgLine.children(".text");

        o.attr("status", status);
    }

    this.__chatSubmit = function(  event ) 
    {
        var c = this._chat;
        var that = this;

        if(!c)
            return false;

        var me = $("#chat-input");
        var msg = me.val().trim();

        if( msg.trim() == "" )
            return;

        me.val("");

        var girlData = this._shownItem.objectData;

        var parseChatReply = function(answer)
        {
            var phone = girlData.Phone+"";
            var repl = "<a " + phoneHref(phone) + ">" + formatPhone(phone) + "</a>";
            return answer.replace(phone, repl);
        }

        // set previous message status to 'delivered'
        if (that._submitLastLine) {
            setMsgStatus(that._submitLastLine, "delivered");
        }

        var lastLine = c.addMsgMy(msg);
        that._submitUndeliveredLines.push(lastLine);
        setMsgStatus(lastLine, "init");
        setTimeout(function(){setMsgStatus(lastLine, "sended")}, 100);


        //c.showTypingAnim(true);
        c.getReply(msg, 
            function(rep) { // success
                that._submitUndeliveredLines.forEach( function(item, i, arr) {
                    setMsgStatus(item, "delivered"); // TODO: set all previous messsages as 'delivered'
                });

                var timeToRead = msg.length * 30;
                var timeToWrite = rep.answer.length * 80;

                var t1 = (c.sessionMessages() > 1) ? timeToRead + rnd(500, 1000) : rnd(3000, 6000);
                var t2 = 2000 + (timeToWrite)*rnd(0.8, 1.3);

                that._submitTimerid = setTimeout( function() {
                    c.showTypingAnim(true);
                    //console.log("chat t1 elapsed " + t1);

                    that._submitTimerid = setTimeout( function(){
                        c.addMsgGirl( parseChatReply(rep.answer) );
                        //console.log("chat t2 elapsed " + t2);
                        c.showTypingAnim(false);

                        that._submitTimerid = null;
                    }, t2 );
                }, t1 );
            },
            function(jqXHR, textStatus, errorThrown) { // err
                //c.showTypingAnim(false);
                //console.log("chat sent error - " + textStatus + ": " + jqXHR);
                setMsgStatus(lastLine, "err");
            },
            function(jqXHR, textStatus) { // complete
                c.showTypingAnim(false);
                //console.log("chat sent fin - " + textStatus);
            }
        );

        //me.val("");
        event.preventDefault();
        event.stopPropagation();
        return false;
    }

    var showOnlyPhoto = function( showPhotoBtn )
    {
        if (showPhotoBtn.hasAttribute("checked")) {
          showPhotoBtn.removeAttribute("checked");
          $(".chat-hideable").removeAttr("hidden");
        }
        else {
          showPhotoBtn.setAttribute("checked",true);
          $(".chat-hideable").attr("hidden", true);
        }
    }

    this.__setupByItem = function(item)
    {
        this._item = item;
        var o = item.objectData;

        var cnt = this._obj;

        cnt.find("#name").text( o.Name );
        cnt.find("#age").text( o.Age );
        cnt.find("#city").text( o.City );
        cnt.find("#moto").text( o.Moto );
        cnt.find("#desc").text( o.Description );
        //cnt.find("#photo").attr( "src", imgSmall2(o, 0, 300) );

        {
            var phone = cnt.find("#phone").text( formatPhone(o.Phone) );

            if (isMobile())
                phone.attr("href", "tel:"+o.Phone );
        }

        cnt.find("#chat-input").focus();

        cnt.find("#lightbox-show-photo").on( "click", function(e) {
            e.preventDefault();
            showOnlyPhoto(this);
        });

        var img = imgFull2(o);
        var photoRef = $(".chat-photo");
        photoRef.css("background-image", "none"); // first - remove from cache
        photoRef.css("background-image", "url(\""+encodeURI(img)+"\")");
    }

    this.__scrollHandler = function(e)
    {
        //if( !$.contains( document.getElementById("lightbox"), e.target) )
        {
            e.stopImmediatePropagation();
            e.preventDefault();
            return false;
        }
    }

    this.show = function(item)
    {
        var lightboxCtrls = $(".chat-smooth-showable, .chat-hideable");

        lightboxCtrls.removeAttr("hidden");
        $("#lightbox-show-photo").attr("checked", false);

        // show chat controls smoothely
        {
            lightboxCtrls.attr("hidden", true);
            setTimeout( function() {
                lightboxCtrls.removeAttr("hidden");
            }, 400 );
        }

        this.__setupByItem(item);
        this._chat.startSession(item);

        var mainCntnt = $("#main-content-section");
        this._prevScrollX = window.scrollX; // mainCntnt.scrollLeft();
        this._prevScrollY = window.scrollY; // mainCntnt.scrollTop();
        mainCntnt.css("display", "none");

        // bulma fixup
        $("html").addClass("is-clipped");
        this._obj.addClass("is-active");
        //this._obj.attr("visible", true);

        this._shownItem = item;

        if (item.objectData.history.length == 0)
        {
            var m2 = this._chat.addMsgGirl( "<i>"+item.objectData.Moto + "</i>&hellip;" );
            $("#chat-input").val("Hei, " + item.objectData.Name + "!");
        }
        else 
        {
            this._chat.restoreHistory(item.objectData.history);
            $("#chat-input").val("");
        }

        $(window).on("scroll touchmove", this.__scrollHandler);

        var that = this;
        setTimeout( function(){that._obj.attr("visible", true);}, 0 )

        this.autoStartChat = setTimeout( function() {
            if (that._chat.sessionMessages() == 0) { // initiate talk
                if (that._shownItem) {
                    $("#chat-button").click();
                }
            }
        }, AUTOSTART_CHAT_TIMEOUT );

        $("#chat-input").off("input");
        $("#chat-input").on("input", function() {
            clearTimeout(that.autoStartChat);
        });
    }

    this.hide = function()
    {
        clearTimeout(this.autoStartChat);

        var mainCntnt = $("#main-content-section");
        mainCntnt.css("display", "block");
        window.scrollTo( this._prevScrollX, this._prevScrollY );

        {
            $(window).off("scroll touchmove");
            this._obj.find("#lightbox-show-photo").off( "click" );
        }

        if (this._item)
        {
            this._item.objectData.history = this._chat.getHistory();
            this._item = null;
        }

        this._chat.stopSession();

        // bulma fixup
        $("html").removeClass("is-clipped");
        this._obj.removeClass("is-active");
        this._obj.removeAttr("visible");

        //closeModals();

        this._shownItem = null;

        this.__cancelReply();
    }

    __init();
    return this;
}