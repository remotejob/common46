function rnd(min_, max_)
{
    var rng = max_-min_;
    return Math.random()*rng + min_
}

// defvice is mobile phone or PAD (not Desctop)
function isMobile()
{
    return /Mobi/.test(navigator.userAgent);
}

function phoneHref(tel)
{
    return isMobile() ? 'href="tel:' + tel + '"' : ''; // 'href="#"';
}

function formatPhone( rawPhone )
{
    return (rawPhone + "").replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
}

function fullImg(thumbImg)
{
    return "img/girl_big.png";
    return thumbImg.replace('thumb','w300shadow');
}

function _imgConverted(uriprefix, op, id, baseName, ct, w, h)
{
    //return "img/girl_small.png";
    return uriprefix + "/display?op="+op+"&" + (w?("w="+w+"&"):"") + (h?("h="+h+"&"):"") + "path=" + id + "/original/" + baseName;
}

function imgResized(uriprefix, id, baseName, ct, w, h)
{
    return _imgConverted(uriprefix, "resize", id, baseName, ct, w, h);
}

function imgThumbnailed(uriprefix, id, baseName, ct, w, h)
{
    return _imgConverted(uriprefix, "thumbnail", id, baseName, ct, w, h);
}

<<<<<<< HEAD
function imgSmall(uriprefix, id, baseName, ct, w, h)
{
    return _imgConverted(uriprefix, "thumbnail", id, baseName, ct, w, h);
=======
    //http://104.131.122.192:3001/display?h=400&path=238/original/b2.jpg&op=resize
    //var r = uriprefix + "/display?op=thumbnail&" + (w?("w="+w+"&"):"") + (h?("h="+h+"&"):"") + "path=" + id + "/original/" + baseName;
    return uriprefix + "/display?op=resize&" + (w?("w="+w+"&"):"") + (h?("h="+h+"&"):"") + "path=" + id + "/original/" + baseName;
>>>>>>> 300fa8fea9f5adc8991ebeb81e38ac0d4f16c255
}

function imgFull(uriprefix, id, baseName)
{
    // http://104.131.122.192:8000/fullimage/238/original/b2.jpg 
    return uriprefix + "/fullimage/" + id + "/original/" + baseName;
}

function imgSmall2(obj, w, h)
{
    return imgSmall( IMG_URL_PREFIX, obj.Id, obj.Img_file_name, obj.Img_content_type, w, h );
}

function imgResized2(obj, w, h)
{
    return imgResized( IMG_URL_PREFIX, obj.Id, obj.Img_file_name, obj.Img_content_type, w, h );
}

function imgThumbnailed2(obj, w, h)
{
    return imgThumbnailed( IMG_URL_PREFIX, obj.Id, obj.Img_file_name, obj.Img_content_type, w, h );
}

function imgFull2(obj)
{
    return imgFull( DATA_URL_PREFIX, obj.Id, obj.Img_file_name );
}

function getImageMeta(url, cb)
{   
    var img = new Image();
    img.addEventListener("load", function(){
        cb( this.naturalWidth, this.naturalHeight );
    });
    img.src = url;
}

function getImageSizeAndAppendItem( cntr, item )
{
    var img = document.createElement("img");
    img.src = imgFull2(item);

    var w = item.naturalWidth;
    var h = item.naturalheight;

    item.imgW = w;
    item.imgH = h;

    cntr.push(item);
}

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}

function timeHHMM(t)
{
  var h = t.getHours()
  var m = t.getMinutes()

  if(m < 10)
    m = "0"+m;

  return h+":"+m;
}

function currTimeHHMM()
{
  return timeHHMM(new Date());
}

function genRandomHexStr(digits)
{
    var b = Math.pow(2, digits)-1;
    return pad(Math.floor(Math.random()*16777215).toString(16), digits);
}


function hashCodeInit()
{
    return 5381;
}

function hashCodeStr(str, hash)
{
    if (typeof hash === 'undefined') {
        hash = 5381;
    }

    for (i = 0; i < str.length; ++i) {
        var char = str.charCodeAt(i);
        hash = ((hash << 5) + hash) + char; // hash * 33 + c
    }
    return hash;
}

function hashCodeInt(val, hash)
{
    if (typeof hash === 'undefined') {
        hash = 5381;
    }

    hash = ((hash << 5) + hash) + val; // hash * 33 + c 

    return hash;
}

