<<<<<<< HEAD
var CHAT_REPLY_TIMEOUT = 20000;


function SexyChatCtrl(windowObj, servicePrefix, params)
{
    this._chat = new ChatWindowCtrl(windowObj, params);
    this._servicePrefix = servicePrefix;

    this._sessionObj = null;
    this._sessionKey = "";
    this._replyQueue = new Array();
    this._sessionMessages = 0;

    this._canSentQuery = true;
    this._canSentQuerySubscribers = new Array();

    this._typingAnimated = false;
    this._typingAnimation = null;
    this._typingAnimationIdx = 0;
    this._typingTotalTime = 0;

    this._reqJqxhr = null;

    var genSessionKey = function()
    {
        var r = 
        genRandomHexStr(8)+"-"+
        genRandomHexStr(4)+"-"+
        genRandomHexStr(4)+"-"+
        genRandomHexStr(4)+"-"+
        genRandomHexStr(12);
        return r;
    }

    this.getReply = function(text, onSuccess, onError, onComplete)
    {
        if( !this.isSessionOpen() )
        {
            console.log("cant sent reply: session is not open");
            return;
        }

        if( !this._canSentQuery )
        {
            console.log("cant sent reply: sentry is set");
            return;
        }

        this._canSentQuery = false;
        var phone = this._sessionObj.objectData.Phone;
        var req = this._servicePrefix + "/chat/" + this._sessionKey + "/" + phone + "/" + escape(text);

        var that = this;

        // get query
        this._reqJqxhr = $.ajax({
            type: "GET",
            url: req,
            timeout: CHAT_REPLY_TIMEOUT,
            dataType : "json",
            context: document.body,
            success: onSuccess,
            error: onError, // error(jqXHR, textStatus, errorThrown)
            complete: function(jqXHR, textStatus) {
                that._canSentQuery = true;
                if(typeof onComplete !== 'undefined')
                    onComplete(jqXHR, textStatus);

                that._reqJqxhr = null;
            }
        });
    }

    this.cancelReply = function()
    {
        if (this._reqJqxhr)
        {
            this._reqJqxhr.abort("abort by user");
        }
    }

    this.startSession = function(item)
    {
        this._chat._history = new Array();
        this._chat.removeAllMessages();
        this._chat.removeAllTypingNotify();
        this._sessionObj = item;
        this._sessionKey = genSessionKey();
        this._sessionMessages = 0;
    }

    this.stopSession = function()
    {
        //this._chat._history.length = 0;
        return this._sessionKey == "";
    }

    this.sessionMessages = function()
    {
        return this._sessionMessages;
    }

    this.isSessionOpen = function()
    {
        return this._sessionKey.length > 0;
    }

    this.addMsgMy = function(html) 
    {
        var el = this._chat.addMessage("my", html, "Sinä");
        //this._chat.scrollToLastMessage(300);
        this.scrollToLastMessage(300)
        this._sessionMessages++;
        return el;
    }

    this.addMsgGirl = function(html) 
    {
        var el = this._chat.addMessage("girl", html, this._sessionObj.objectData.Name);
        //this._chat.scrollToLastMessage(300);
        this.scrollToLastMessage(300)
        return el;
    }

    this.addMsgSpace = function(html) 
    {
        var el = this._chat.addMessageTimed("space", html, "", "");
        return el;
    }

    this.scrollToLastMessage = function(timeToScroll)
    {
        this._chat.scrollToLastMessage(timeToScroll);
        return;

        var that = this;
        setTimeout( function() { 
            that._chat.scrollToLastMessage(timeToScroll);
        }, 0 );
    }

    this.restoreHistory = function(history)
    {
        this._chat.restoreHistory(history);
    }

    this.getHistory = function()
    {
        return this._chat.getHistory();
    }

    this.showTypingAnim = function(show) 
    {
        if( show ) {
            var n = this._sessionObj ? this._sessionObj.objectData.Name : "";
            this._chat.addTypingNotify("girl", n + "<div class='hellip'>&nbsp;&hellip;</div>")
        }
        else {
            this._chat.removeTypingNotify("girl");
        }
=======
function ChatCtrl(windowObj, params)
{
    // first, clear container
    if( windowObj )
    {
        windowObj.empty();
    }

    this.obj = windowObj;

    this.classMe = params.me;
    this.classGirl = params.girl;
    this.classTyping = params.typing;
    this._typingObj = $(document.createElement("div")).addClass(this.classTyping).css("display", "none").text("typing...");
    windowObj.append(this._typingObj);
    this._typingUpdate = null; 
    this._typingUpdateIdx = 0; 
    //this._typingObjShown = false;

    this._addEl = function(eClass, eText, eAttrs) 
    {
        var o = $(document.createElement("div")).addClass(eClass).html(eText);
        //var o = $("<div class='" +this.msgClass+ "'>"+text+"</div>");

        if( typeof eAttrs !== 'undefined')
        {
            for(i in eAttrs) 
            {
                o.attr(eAttrs[i].name, eAttrs[i].value);
            }
        }

        //windowObj.append(o);
        this._typingObj.before(o);
        return o;
    }

    this.addMsg = function(msgClass, text) 
    {
        return this._addEl(msgClass, text);
    }

    this.addMsgMy = function(text) 
    {
        return this._addEl(this.classMe, text);
    }

    this.addMsgGirl = function(text) 
    {
        return this._addEl(this.classGirl, text);
    }

    this.showTyping = function(show) 
    {
        //var shown = this._typingObj != null;
        //if( show == shown )
        //    return;

        this._typingObjShown = true;

        if( show )
        {
            var puper = this; // propaganate this to timeout callback
            this._typingObj.css("display", "block");
            this._typingUpdateIdx = 0;
            this._typingUpdate = setInterval( function() {
                var i = puper._typingUpdateIdx++;
                puper._typingObj.text("typing 3" + ".".repeat(i%4));
            }, 400);
        }
        else
        {
            this._typingObj.css("display", "none");
            clearInterval(this._typingUpdate);
            this._typingUpdate = null;
        }

        //this.obj.append(o);
    }

    return this;
}

function SexyChatCtrl(servicePrefix, girlObj, windowObj, params)
{
    this.chat = new ChatCtrl(windowObj, params);
    this.phone = girlObj.Phone;
    this.servicePrefix = servicePrefix;

    this._canSentQuery = true;
    this._canSentQuerySubscribers = new Array();

    this.getReply = function(text, onSuccess, onError, onComplete)
    {
        this._canSentQuery = false;

        var ckey = "316451ed-e28a-420a-f566-0987144939b5";
        var req = this.servicePrefix + "/chat/" + ckey + "/" + this.phone + "/" + escape(text);
        // http://www.omakuva.fi:8000/chat/316451ed-e28a-420a-f566-0987144939b5/0600411466/Hei%20Tuula!

        // get query
        $.ajax({
            type: "GET",
            url: req,
            dataType : "json",
            context: document.body,
            success: onSuccess,
            error: onError, // error(jqXHR, textStatus, errorThrown)
            complete: function(jqXHR, textStatus) {
                this._canSentQuery = true;
                if(typeof onComplete !== 'undefined')
                    onComplete(jqXHR, textStatus);
            }
        });
>>>>>>> 300fa8fea9f5adc8991ebeb81e38ac0d4f16c255
    }

    var canSentQuery = function()
    {
        return _canSentQuery;
    }

    return this;
<<<<<<< HEAD
}

=======
}
>>>>>>> 300fa8fea9f5adc8991ebeb81e38ac0d4f16c255
