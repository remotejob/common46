function markLoaded(img)
{
  img.addAttribute("loaded",true);
}

// return string for represent rating of [1..10]
// 5 stars
/*function ratingOf10(r)
{
  var star = "\f005";
  var half = "\f123";
  var none = "\f006";

  var s;

  for(var i = 0; i < 10; ++i)
  {
    if (r > 0) {
      s += (r > 1) ? star : half;
    }
    else {
      s += none;
    }
    r -= 2;
  }
}*/

function calcRatingOf10(item)
{
  var h = hashCodeInit();
  h = hashCodeStr(item.Name, h);
  h = hashCodeStr(item.Moto, h);
  h = hashCodeInt(item.Age, h);

  var r = 6 + Math.abs(h) % 5;
  return r;
}

function GirlList(obj)
{
    this._obj = obj; // container (jquery object)
    this.items = new Array();
    this.__query = null;
    this.itemsReady = 0; // then fully loaded itemsReady == items.length

    this.__scrollHandler = function()
    {

    }

    this.__validateItem = function(i)
    {
        var isOk = function(v)
        {

            return typeof v !== 'undefined' && v !== null;
        }

        if( isOk(i.Id) &&
            isOk(i.Name) &&
            isOk(i.Age) &&
            isOk(i.Moto) &&
            isOk(i.Description) &&
            isOk(i.City) &&
            isOk(i.Region_id) &&
            isOk(i.Phone) &&
            isOk(i.Adv_phone_id) &&
            isOk(i.Topic) && 
            isOk(i.Sex) && 
            isOk(i.Img_file_name) 
            )
        {
            return true;
        }

        return false;
    }

    this.__processLoadedItemArray = function(arr)
    {
        for(var key in arr)
        {
            var o = arr[key];
            if( this.__validateItem(o) ) {
                this.__processLoadedItem(key, o);
            }
        } 

        this._obj.attr("builded", true);
    }

    this.__processLoadedItem = function(idx, i)
    {
    /*
    "Id": 238,
    "Name": "Sirja",
    "Age": 25,
    "Moto": "Seksinovelli mulkut",
    "Description": "Karvainen pillu...",
    "City": "Mikkeli",
    "Region_id": 17,
    "Phone": "0700411342",
    "Adv_phone_id": 86,
    "Img_orient": "portrait",
    "Topic": "sex",
    "Sex": "female",
    "Created_at": "2012-08-01T15:45:31Z",
    "Updated_at": "2012-08-01T15:45:31Z",
    "Img_file_name": "b2.jpg",
    "Img_content_type": "image/jpeg",
    "Img_file_size": 44643,
    "Img_updated_at": "2012-08-01T15:45:29Z"

    ---
    imgW
    imgH
    imgObj -- Image() object 
    obj -- main container
    */


        var url = imgFull2(i);
        var that = this;

        i.imgW = 1;
        i.imgH = 1;

            //var isPortrait = i.Img_orient == "portrait";
            var imgW = 250;
            var imgH = 250; //i.imgW ? Math.round(imgW*i.imgH/i.imgW) : 0;
            var urlSmall = imgSmall2(i, imgW, imgH);
            var rotDeg = Math.random()*8-4;

    var code = 
    '<div class="card" style="transform: rotate('+rotDeg+'deg);"> \
      <div class="card-image">\
        <figure class="image">\
          <img class="card-image-img" src="" alt="Placeholder image" width="'+ imgW +'" height="'+ imgH +'">\
        </figure>\
      </div>\
      <div class="card-content">\
        <div class="media">\
          <div class="media-content">\
            <p class="title is-4" rating="'+calcRatingOf10(i)+'">'+ i.Name + ' (' + i.Age +'v)</p>\
            <p class="subtitle is-5">@' + i.City + ' <a class="phone photo-phone" ' + phoneHref(i.Phone) +'>'+ formatPhone(i.Phone) +'</a></p>\
          </div>\
        </div>\
    \
      </div>\
    </div>';

        var obj = $(code);
        obj.objectData = i;
        obj.find(".card-image-img").on("click", function(){onBabeClicked(obj,i)});
        obj.find(".card-image-img").on("load", function() { this.setAttribute("loaded", true); } );
        i.obj = obj;

        that._obj.append(obj);

        i.imgObj = new Image();
        i.imgObj.addEventListener("load", function(){
            i.imgW = this.naturalWidth;
            i.imgH = this.naturalHeight;

            that.itemsReady++;

            obj.find(".card-image-img").attr("src", urlSmall);
        });
        i.imgObj.src = url;
        i.history = new Array(); // chat history

        this.items.push(i);
    }

    this.clear = function()
    {
        this.itemsReady = 0;

        // cancel all query (if any)
        if( this.__query ) {
            this.__query.abort();
            this.__query = null; 
        }

        for(var i in this.items)
        {
            var o = this.items[i];

            if( o.imgObj ) {
                o.imgObj = null; // remove event listeners
            }
        }

        this.items.length = 0; // clear items;
    }

    this.load = function(req)
    {
        var that = this;

        this.clear();

        this.__query = $.ajax({
            type: "GET",
            url: req, //DATA_URL_PREFIX+"/api",
            dataType : "json",
            context: document.body,
            success: function(data) {
                that.__processLoadedItemArray(data);
            },
            error: function() {
            },
            complete: function() {
            }
        });
    }


    var __init = function()
    {

    }

    __init();
    return this;
}