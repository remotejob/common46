// css classes
// .messages
// .typings
// .msgline
// .msg, .time

// <ul class=typings>
//   <li class="girl1">Lola typing...</li>
//   <li class="girl2">Iris typing...</li>
// </ul>
//
// <ul class=messages>
//   <li class="msg user1">
//      <div class="msgline">
//          <div class="text">Hola baby!</div>
//          <div class="time">12:45/div>
//      </div>
//   </li>
// </ul>
//



function ChatWindowCtrl(windowObj, params)
{
    // first, clear container
    if( windowObj )
    {
        windowObj.empty();
    }

    this._obj = windowObj;
    this._objMessages = $(document.createElement("ul")).addClass("messages");
    this._objTypings  = $(document.createElement("ul")).addClass("typings");
    this._history = new Array();

    this._obj.append(this._objTypings);
    this._obj.append(this._objMessages);

    this.addTypingNotify = function(typingClass, textHtml)
    {
        var curr = this._objTypings.children("."+typingClass);

        if( !(curr && curr.hasClass(typingClass)) )
        {
            var o = $(document.createElement("li")).addClass(typingClass);
            if(textHtml)
                o.html(textHtml);

            this._objTypings.append( o );
            return o;
        }

        return null;
    }

    this.removeTypingNotify = function(typingClass)
    {
        return this._objTypings.children("."+typingClass).remove();
    }

    this.removeAllTypingNotify = function()
    {
        return this._objTypings.empty();
    }

    this.restoreHistory = function(history)
    {
        this.removeAllMessages();
        this._history.length = 0;

        for(i in history)
        {
            var item = history[i];
            this.addMessageTimed(item.name, item.html, item.title, item.time);
        }

        this.scrollToLastMessage(300);
        this._sessionMessages == history.length;
    }

    this.getHistory = function()
    {
        return this._history;
    }

    this._ensureMessageBundleForClass = function(messageClass, definitionHtml)
    {
        var l = this._objMessages.children().last();

        if( l && l.hasClass(messageClass) )
        {
            return l;
        }
        else 
        {
            var n = $(document.createElement("li")).addClass("msg").addClass(messageClass);
            var d = $(document.createElement("dd")).html(definitionHtml);
            n.append(d);
            this._objMessages.append(n);
            return n;
        }
    }

    this.addMessageTimed = function(eClass, messageHtml, titleHtml, timeStr, eAttrs) 
    {
        this._history.push( {"name":eClass, "html":messageHtml, "title":titleHtml, "time":timeStr} );

        var line = $(document.createElement("div")).addClass("msgline").addClass("columns");
        var om = $(document.createElement("div")).addClass("text").html(messageHtml);
        var ot = $(document.createElement("div")).addClass("time").html(timeStr);

        ot.addClass("small");

        line.append(om);
        line.append(ot);

        if( typeof eAttrs !== 'undefined')
        {
            for(i in eAttrs) 
            {
                om.attr(eAttrs[i].name, eAttrs[i].value);
            }
        }

        this._ensureMessageBundleForClass(eClass,titleHtml).append(line);

        setTimeout( function(){ line.attr("created", true); }, 0 )

        return line;
    }

    this.addMessage = function(eClass, messageHtml, titleHtml, eAttrs) 
    {
        return this.addMessageTimed(eClass, messageHtml, titleHtml, currTimeHHMM(), eAttrs);
    }

    this.removeAllMessages = function()
    {
        this._objMessages.empty();
        this._objMessages.remove("li");
    }

    this.scrollToLastMessage = function(speed)
    {
        var scrollContainer = this._obj;
        //var scrollContainer = this._obj.parent().parent().parent().parent().parent(); // "#chatScroller");

        var l = this._objMessages.children().last();
        var ll = l.children().last();
        var pos = ll.offset().top - scrollContainer.offset().top + scrollContainer.scrollTop();

        scrollContainer.animate({ scrollTop: pos }, speed);
    }

    return this;
}
